const Pool = require('pg').Pool;
const pool = new Pool({
  host: process.env.HOST,
  user: process.env.USER,
  database: process.env.DB,
  password: process.env.PASSWORD,
  port: 5432
});


async function createTableUsers() {
  const client = await pool.connect();

  try {
    const createTableQuery = `
      CREATE TABLE IF NOT EXISTS public.users (
        id SERIAL PRIMARY KEY,
        name VARCHAR(30),
        email VARCHAR(30) UNIQUE
      );
    `;

    await client.query(createTableQuery);
    console.log('Table "users" created successfully.');
  } catch (error) {
    console.error('Error creating table:', error);
  } finally {
    client.release(); // Release the client back to the pool
  }
}

async function insertData() {
  const client = await pool.connect();

  const dataToInsert = [
    { name: 'Ismael Limo', email: 'ismael.limo@gmail.com' },
    { name: 'Catalina', email: 'catalina@gmail.com' }
  ];

  try {
    for (const row of dataToInsert) {
      await client.query(`
        INSERT INTO public.users (name, email)
        VALUES ($1, $2)
        ON CONFLICT (email) DO NOTHING;
      `, [row.name, row.email]);
    }

    console.log('Data inserted successfully, duplicates prevented.');
  } catch (error) {
    console.error('Error inserting data:', error);
  } finally {
    client.release(); // Release the client back to the pool
  }
}

const getUsers = (request, response) => {
  pool.query('SELECT * FROM users ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

const getUserById = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query('SELECT * FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

const createUser = (request, response) => {
  const { name, email } = request.body;

  pool.query(
    'INSERT INTO users (name, email) VALUES ($1, $2) RETURNING *',
    [name, email],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(201).send(`User added with ID: ${results.rows[0].id}`);
    }
  );
};

const updateUser = (request, response) => {
  const id = parseInt(request.params.id);
  const { name, email } = request.body;

  pool.query(
    'UPDATE users SET name = $1, email = $2 WHERE id = $3',
    [name, email, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(200).send(`User modified with ID: ${id}`);
    }
  );
};

const deleteUser = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).send(`User deleted with ID: ${id}`);
  });
};

module.exports = {
  createTableUsers,
  insertData,
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
};
