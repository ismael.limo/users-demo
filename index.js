const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

const db = require('./queries');

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

db.createTableUsers()
  .then(() => {
    db.insertData()
    .then(() => {
      
    })
    .catch((error) => {
      console.error('Error:', error);
    });
    
  })
  .catch((error) => {
    console.error('Error creating table:', error);
  });




app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' });
});

app.get('/users', db.getUsers);
app.get('/users/:id', db.getUserById);
app.post('/users', db.createUser);
app.put('/users/:id', db.updateUser);
app.delete('/users/:id', db.deleteUser);

app.listen(port, () => {
  console.log(`App is running on port ${port}`);
});